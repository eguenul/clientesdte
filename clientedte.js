const axios = require('axios');

async function enviarJson() {
    const apiUrl = 'http://170.239.87.203:8080/appdtews/api/sendDTE'; // Reemplaza con la URL de tu servicio REST

    const jsonData = getJsonData();
    console.log('JSON enviado:');
    console.log(jsonData);

    try {
        const response = await axios.post(apiUrl, jsonData, {
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            }
        });

        console.log('Respuesta del servicio:');
        console.log(response.data);
    } catch (error) {
        console.error('Error al realizar la solicitud HTTP:', error.message);
    }
}

function getJsonData() {
    const detalles = [];

    for (let i = 1; i <= 3; i++) {
        detalles.push({
            nrolinea: i.toString(),
            cdgitem: [
                { tpocodigo: 'MONEDA', vlrcodigo: '013' },
                { tpocodigo: 'TRANSACC', vlrcodigo: 'VENTA' }
            ],
            undmditem: 'UN',
            nmbitem: 'DOLAR USA',
            qtyitem: '140315',
            prcitem: '904',
            descuentopct: '0',
            descuentomonto: '0',
            indexe: i.toString(),
            montoitem: '126844760'
        });
    }

    const jsonData = {
        emisor: {
            rutemisor: '76040308-3',
            rsemisor: 'EGGA INFORMATICA',
            giroemisor: 'SERVICIOS DE INGENIERIA',
            actecoemisor: '429000',
            diremisor: 'quillay 467 villa presidente rio',
            cmnaemisor: 'TALCAHUANO',
            ciuemisor: 'CONCEPCION',
            fecharesol: '2016-04-25',
            numresol: '0',
            codsiisucur: '1',
            rutenvia: '13968481-8'
        },
        receptor: {
            rutreceptor: '77813960-K',
            rsreceptor: 'AMULEN CONSULTORES LTDA',
            giroreceptor: 'ASESORIA TRIBUTARIA',
            dirreceptor: 'URMENETA 305 OFIC 512',
            cmnareceptor: 'puerto montt',
            ciureceptor: 'puerto montt',
            rutcaratula: '60803000-K'
        },
        iddoc: {
            tipodte: '34',
            numdte: '1',
            fechaemision: '2024-01-29',
            frmapago: '1'
        },
        referencia: {
            codref: '0',
            razonref: 'DOCUMENTO DE PRUEBAS',
            numref: '1',
            tpodocref: '801',
            folioref: '1',
            fecharef: '2022-09-01'
        },
        totales: {
            montoexento: '126844760',
            montototal: '126844760'
        },
        detalle: detalles,
        usuario: {
            login: 'eguenul',
            rut: '13968481-8',
            password: 'amulen1956'
        }
    };

    return JSON.stringify(jsonData);
}

enviarJson();
