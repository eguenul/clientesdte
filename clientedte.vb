Imports System.Net.Http
Imports System.Net.Http.Json
Imports System.Threading.Tasks
Imports Newtonsoft.Json

Module Programdotne
    Sub Main()
        Dim jsonData As String = GetJsonData()
        Console.WriteLine("JSON enviado:")
        Console.WriteLine(jsonData)

        Dim apiUrl As String = "http://localhost:8080/appdtews/api/sendDTE" ' Reemplaza con la URL de tu servicio REST
        Dim response = EnviarJson(apiUrl, jsonData).Result

        Console.WriteLine("Respuesta del servicio:")
        Console.WriteLine(response)



    'leer la recepcion
        Dim datos As DatosJson = JsonConvert.DeserializeObject(Of DatosJson)(response)
        Console.WriteLine("Rut Emisor: " & datos.rut_emisor)
        Console.WriteLine("Rut Envia: " & datos.rut_envia)
        Console.WriteLine("Track ID: " & datos.trackid)
        Console.WriteLine("Fecha Recepcion: " & datos.fecha_recepcion)
        Console.WriteLine("Estado: " & datos.estado)
        Console.WriteLine("File: " & datos.file)
        ' Puedes acceder a los datos según sea necesario.













    End Sub

    Private Function GetJsonData() As String
        ' Crear y retornar el JSON con una cantidad dinámica de detalles
        Dim detalles As New List(Of DetalleItem)

        For i As Integer = 1 To 3 ' Cambia el número según la cantidad de detalles necesarios
            detalles.Add(New DetalleItem With {
                .nrolinea = i.ToString(),
                .cdgitem = New List(Of CodigoItem) From {
                    New CodigoItem With {.tpocodigo = "MONEDA", .vlrcodigo = "013"},
                    New CodigoItem With {.tpocodigo = "TRANSACC", .vlrcodigo = "VENTA"}
                },
                .undmditem = "UN",
                .nmbitem = "DOLAR USA",
                .qtyitem = "140315",
                .prcitem = "904",
                .descuentopct = "0",
                .descuentomonto = "0",
                .indexe = i.ToString(),
                .montoitem = "126844760"
            })
        Next

        Dim jsonData As New JsonData With {
            .emisor = New EmisorItem With {
                .rutemisor = "76040308-3",
                .rsemisor = "EGGA INFORMATICA",
                .giroemisor = "SERVICIOS DE INGENIERIA",
                .actecoemisor = "429000",
                .diremisor = "quillay 467 villa presidente rio",
                .cmnaemisor = "TALCAHUANO",
                .ciuemisor = "CONCEPCION",
                .fecharesol = "2016-04-25",
                .numresol = "0",
                .codsiisucur = "1",
                .rutenvia = "13968481-8"
            },
            .receptor = New ReceptorItem With {
                .rutreceptor = "77813960-K",
                .rsreceptor = "AMULEN CONSULTORES LTDA",
                .giroreceptor = "ASESORIA TRIBUTARIA",
                .dirreceptor = "URMENETA 305 OFIC 512",
                .cmnareceptor = "puerto montt",
                .ciureceptor = "puerto montt",
                .rutcaratula = "60803000-K"
            },
            .iddoc = New IdDocItem With {
                .tipodte = "34",
                .numdte = "3",
                .fechaemision = "2024-01-29",
                .frmapago = "1"
            },
            .referencia = New ReferenciaItem With {
                .codref = "0",
                .razonref = "DOCUMENTO DE PRUEBAS",
                .numref = "1",
                .tpodocref = "801",
                .folioref = "1",
                .fecharef = "2022-09-01"
            },
            .totales = New TotalesItem With {
                .montoexento = "126844760",
                .montototal = "126844760"
            },
            .detalle = detalles,
            .usuario = New UsuarioItem With {
                .login = "eguenul",
                .rut = "13968481-8",
                .password = "amulen1956"
            }
        }

        Return JsonConvert.SerializeObject(jsonData)
    End Function

    Private Async Function EnviarJson(apiUrl As String, jsonData As String) As Task(Of String)
        Using httpClient As New HttpClient()
            Dim content As New StringContent(jsonData, System.Text.Encoding.UTF8, "application/json")
            Dim response As HttpResponseMessage = Await httpClient.PostAsync(apiUrl, content)
            Return Await response.Content.ReadAsStringAsync()
        End Using
    End Function
End Module

' Clases necesarias para definir la estructura del JSON
Public Class JsonData
    Public Property emisor As EmisorItem
    Public Property receptor As ReceptorItem
    Public Property iddoc As IdDocItem
    Public Property referencia As ReferenciaItem
    Public Property totales As TotalesItem
    Public Property detalle As List(Of DetalleItem)
    Public Property usuario As UsuarioItem
End Class

Public Class EmisorItem
    Public Property rutemisor As String
    Public Property rsemisor As String
    Public Property giroemisor As String
    Public Property actecoemisor As String
    Public Property diremisor As String
    Public Property cmnaemisor As String
    Public Property ciuemisor As String
    Public Property fecharesol As String
    Public Property numresol As String
    Public Property codsiisucur As String
    Public Property rutenvia As String
End Class

Public Class ReceptorItem
    Public Property rutreceptor As String
    Public Property rsreceptor As String
    Public Property giroreceptor As String
    Public Property dirreceptor As String
    Public Property cmnareceptor As String
    Public Property ciureceptor As String
    Public Property rutcaratula As String
End Class

Public Class IdDocItem
    Public Property tipodte As String
    Public Property numdte As String
    Public Property fechaemision As String
    Public Property frmapago As String
End Class

Public Class ReferenciaItem
    Public Property codref As String
    Public Property razonref As String
    Public Property numref As String
    Public Property tpodocref As String
    Public Property folioref As String
    Public Property fecharef As String
End Class

Public Class TotalesItem
    Public Property montoexento As String
    Public Property montototal As String
End Class

Public Class DetalleItem
    Public Property nrolinea As String
    Public Property cdgitem As List(Of CodigoItem)
    Public Property undmditem As String
    Public Property nmbitem As String
    Public Property qtyitem As String
    Public Property prcitem As String
    Public Property descuentopct As String
    Public Property descuentomonto As String
    Public Property indexe As String
    Public Property montoitem As String
End Class

Public Class CodigoItem
    Public Property tpocodigo As String
    Public Property vlrcodigo As String
End Class

Public Class UsuarioItem
    Public Property login As String
    Public Property rut As String
    Public Property password As String
End Class



Public Class DatosJson
    Public Property rut_emisor As String
    Public Property rut_envia As String
    Public Property trackid As String
    Public Property fecha_recepcion As String
    Public Property estado As String
    Public Property file As String
End Class
