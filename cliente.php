<?php

class EmisorItem {
    public $rutemisor;
    public $rsemisor;
    public $giroemisor;
    public $actecoemisor;
    public $diremisor;
    public $cmnaemisor;
    public $ciuemisor;
    public $fecharesol;
    public $numresol;
    public $codsiisucur;
    public $rutenvia;

    public function __construct($rutemisor, $rsemisor, $giroemisor, $actecoemisor, $diremisor, $cmnaemisor, $ciuemisor, $fecharesol, $numresol, $codsiisucur, $rutenvia) {
        $this->rutemisor = $rutemisor;
        $this->rsemisor = $rsemisor;
        $this->giroemisor = $giroemisor;
        $this->actecoemisor = $actecoemisor;
        $this->diremisor = $diremisor;
        $this->cmnaemisor = $cmnaemisor;
        $this->ciuemisor = $ciuemisor;
        $this->fecharesol = $fecharesol;
        $this->numresol = $numresol;
        $this->codsiisucur = $codsiisucur;
        $this->rutenvia = $rutenvia;
    }
}

class ReceptorItem {
    public $rutreceptor;
    public $rsreceptor;
    public $giroreceptor;
    public $dirreceptor;
    public $cmnareceptor;
    public $ciureceptor;
    public $rutcaratula;

    public function __construct($rutreceptor, $rsreceptor, $giroreceptor, $dirreceptor, $cmnareceptor, $ciureceptor, $rutcaratula) {
        $this->rutreceptor = $rutreceptor;
        $this->rsreceptor = $rsreceptor;
        $this->giroreceptor = $giroreceptor;
        $this->dirreceptor = $dirreceptor;
        $this->cmnareceptor = $cmnareceptor;
        $this->ciureceptor = $ciureceptor;
        $this->rutcaratula = $rutcaratula;
    }
}

class IdDocItem {
    public $tipodte;
    public $numdte;
    public $fechaemision;
    public $frmapago;

    public function __construct($tipodte, $numdte, $fechaemision, $frmapago) {
        $this->tipodte = $tipodte;
        $this->numdte = $numdte;
        $this->fechaemision = $fechaemision;
        $this->frmapago = $frmapago;
    }
}

class ReferenciaItem {
    public $codref;
    public $razonref;
    public $numref;
    public $tpodocref;
    public $folioref;
    public $fecharef;

    public function __construct($codref, $razonref, $numref, $tpodocref, $folioref, $fecharef) {
        $this->codref = $codref;
        $this->razonref = $razonref;
        $this->numref = $numref;
        $this->tpodocref = $tpodocref;
        $this->folioref = $folioref;
        $this->fecharef = $fecharef;
    }
}

class TotalesItem {
    public $montoexento;
    public $montototal;

    public function __construct($montoexento, $montototal) {
        $this->montoexento = $montoexento;
        $this->montototal = $montototal;
    }
}

class DetalleItem {
    public $nrolinea;
    public $cdgitem;
    public $undmditem;
    public $nmbitem;
    public $qtyitem;
    public $prcitem;
    public $descuentopct;
    public $descuentomonto;
    public $indexe;
    public $montoitem;

    public function __construct($nrolinea, $cdgitem, $undmditem, $nmbitem, $qtyitem, $prcitem, $descuentopct, $descuentomonto, $indexe, $montoitem) {
        $this->nrolinea = $nrolinea;
        $this->cdgitem = $cdgitem;
        $this->undmditem = $undmditem;
        $this->nmbitem = $nmbitem;
        $this->qtyitem = $qtyitem;
        $this->prcitem = $prcitem;
        $this->descuentopct = $descuentopct;
        $this->descuentomonto = $descuentomonto;
        $this->indexe = $indexe;
        $this->montoitem = $montoitem;
    }
}

class CodigoItem {
    public $tpocodigo;
    public $vlrcodigo;

    public function __construct($tpocodigo, $vlrcodigo) {
        $this->tpocodigo = $tpocodigo;
        $this->vlrcodigo = $vlrcodigo;
    }
}

class UsuarioItem {
    public $login;
    public $rut;
    public $password;

    public function __construct($login, $rut, $password) {
        $this->login = $login;
        $this->rut = $rut;
        $this->password = $password;
    }
}

class JsonData {
    public $emisor;
    public $receptor;
    public $iddoc;
    public $referencia;
    public $totales;
    public $detalle;
    public $usuario;

    public function __construct($emisor, $receptor, $iddoc, $referencia, $totales, $detalle, $usuario) {
        $this->emisor = $emisor;
        $this->receptor = $receptor;
        $this->iddoc = $iddoc;
        $this->referencia = $referencia;
        $this->totales = $totales;
        $this->detalle = $detalle;
        $this->usuario = $usuario;
    }
}

function getJsonData() {
    $detalles = array();

    for ($i = 1; $i <= 3; $i++) {
        $detalles[] = new DetalleItem(
            strval($i),
            array(new CodigoItem("MONEDA", "013"), new CodigoItem("TRANSACC", "VENTA")),
            "UN",
            "DOLAR USA",
            "140315",
            "904",
            "0",
            "0",
            strval($i),
            "126844760"
        );
    }

    $jsonData = new JsonData(
        new EmisorItem("76040308-3", "EGGA INFORMATICA", "SERVICIOS DE INGENIERIA", "429000",
            "quillay 467 villa presidente rio", "TALCAHUANO", "CONCEPCION", "2016-04-25", "0", "1", "13968481-8"),
        new ReceptorItem("77813960-K", "AMULEN CONSULTORES LTDA", "ASESORIA TRIBUTARIA", "URMENETA 305 OFIC 512",
            "puerto montt", "puerto montt", "60803000-K"),
        new IdDocItem("34", "1", "2024-01-29", "1"),
        new ReferenciaItem("0", "DOCUMENTO DE PRUEBAS", "1", "801", "1", "2022-09-01"),
        new TotalesItem("126844760", "126844760"),
        $detalles,
        new UsuarioItem("eguenul", "13968481-8", "amulen1956")
    );

    $json = json_encode($jsonData, JSON_PRETTY_PRINT);
    return $json;
}

function enviarJson($apiUrl, $jsonData) {
    $client = new \GuzzleHttp\Client();
    
    $response = $client->request('POST', $apiUrl, [
        'headers' => [
            'Content-Type' => 'application/json',
        ],
        'body' => $jsonData,
    ]);

    return $response->getBody();
}

$jsonData = getJsonData();
echo "JSON enviado:\n";
echo $jsonData;

$apiUrl = "http://170.239.87.203:8080/appdtews/api/sendDTE";
$response = enviarJson($apiUrl, $jsonData);

echo "Respuesta del servicio:\n";
echo $response;

?>
