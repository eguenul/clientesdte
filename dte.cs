using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;

class Program
{
    static async Task Main()
    {
        string jsonData = GetJsonData();
        Console.WriteLine("JSON enviado:");
        Console.WriteLine(jsonData);

        string apiUrl = "http://localhost:8080/appdtews/api/sendDTE"; // Reemplaza con la URL de tu servicio REST
        string response = await EnviarJson(apiUrl, jsonData);

        Console.WriteLine("Respuesta del servicio:");
        Console.WriteLine(response);
    }

    private static string GetJsonData()
    {
        // Crear y retornar el JSON con una cantidad dinámica de detalles
        List<DetalleItem> detalles = new List<DetalleItem>();

        for (int i = 1; i <= 3; i++) // Cambia el número según la cantidad de detalles necesarios
        {
            detalles.Add(new DetalleItem
            {
                nrolinea = i.ToString(),
                cdgitem = new List<CodigoItem>
                {
                    new CodigoItem { tpocodigo = "MONEDA", vlrcodigo = "013" },
                    new CodigoItem { tpocodigo = "TRANSACC", vlrcodigo = "VENTA" }
                },
                undmditem = "UN",
                nmbitem = "DOLAR USA",
                qtyitem = "140315",
                prcitem = "904",
                descuentopct = "0",
                descuentomonto = "0",
                indexe = i.ToString(),
                montoitem = "126844760"
            });
        }

        JsonData jsonData = new JsonData
        {
            emisor = new EmisorItem
            {
                rutemisor = "76040308-3",
                rsemisor = "EGGA INFORMATICA",
                giroemisor = "SERVICIOS DE INGENIERIA",
                actecoemisor = "429000",
                diremisor = "quillay 467 villa presidente rio",
                cmnaemisor = "TALCAHUANO",
                ciuemisor = "CONCEPCION",
                fecharesol = "2016-04-25",
                numresol = "0",
                codsiisucur = "1",
                rutenvia = "13968481-8"
            },
            receptor = new ReceptorItem
            {
                rutreceptor = "77813960-K",
                rsreceptor = "AMULEN CONSULTORES LTDA",
                giroreceptor = "ASESORIA TRIBUTARIA",
                dirreceptor = "URMENETA 305 OFIC 512",
                cmnareceptor = "puerto montt",
                ciureceptor = "puerto montt",
                rutcaratula = "60803000-K"
            },
            iddoc = new IdDocItem
            {
                tipodte = "34",
                numdte = "3",
                fechaemision = "2024-01-29",
                frmapago = "1"
            },
            referencia = new ReferenciaItem
            {
                codref = "0",
                razonref = "DOCUMENTO DE PRUEBAS",
                numref = "1",
                tpodocref = "801",
                folioref = "1",
                fecharef = "2022-09-01"
            },
            totales = new TotalesItem
            {
                montoexento = "126844760",
                montototal = "126844760"
            },
            detalle = detalles,
            usuario = new UsuarioItem
            {
                login = "eguenul",
                rut = "13968481-8",
                password = "amulen1956"
            }
        };

        return JsonConvert.SerializeObject(jsonData);
    }

    private static async Task<string> EnviarJson(string apiUrl, string jsonData)
    {
        using (HttpClient httpClient = new HttpClient())
        {
            StringContent content = new StringContent(jsonData, System.Text.Encoding.UTF8, "application/json");
            HttpResponseMessage response = await httpClient.PostAsync(apiUrl, content);
            return await response.Content.ReadAsStringAsync();
        }
    }
}

public class JsonData
{
    public EmisorItem emisor { get; set; }
    public ReceptorItem receptor { get; set; }
    public IdDocItem iddoc { get; set; }
    public ReferenciaItem referencia { get; set; }
    public TotalesItem totales { get; set; }
    public List<DetalleItem> detalle { get; set; }
    public UsuarioItem usuario { get; set; }
}

public class EmisorItem
{
    public string rutemisor { get; set; }
    public string rsemisor { get; set; }
    public string giroemisor { get; set; }
    public string actecoemisor { get; set; }
    public string diremisor { get; set; }
    public string cmnaemisor { get; set; }
    public string ciuemisor { get; set; }
    public string fecharesol { get; set; }
    public string numresol { get; set; }
    public string codsiisucur { get; set; }
    public string rutenvia { get; set; }
}

public class ReceptorItem
{
    public string rutreceptor { get; set; }
    public string rsreceptor { get; set; }
    public string giroreceptor { get; set; }
    public string dirreceptor { get; set; }
    public string cmnareceptor { get; set; }
    public string ciureceptor { get; set; }
    public string rutcaratula { get; set; }
}

public class IdDocItem
{
    public string tipodte { get; set; }
    public string numdte { get; set; }
    public string fechaemision { get; set; }
    public string frmapago { get; set; }
}

public class ReferenciaItem
{
    public string codref { get; set; }
    public string razonref { get; set; }
    public string numref { get; set; }
    public string tpodocref { get; set; }
    public string folioref { get; set; }
    public string fecharef { get; set; }
}

public class TotalesItem
{
    public string montoexento { get; set; }
    public string montototal { get; set; }
}

public class DetalleItem
{
    public string nrolinea { get; set; }
    public List<CodigoItem> cdgitem { get; set; }
    public string undmditem { get; set; }
    public string nmbitem { get; set; }
    public string qtyitem { get; set; }
    public string prcitem { get; set; }
    public string descuentopct { get; set; }
    public string descuentomonto { get; set; }
    public string indexe { get; set; }
    public string montoitem { get; set; }
}

public class CodigoItem
{
    public string tpocodigo { get; set; }
    public string vlrcodigo { get; set; }
}

public class UsuarioItem
{
    public string login { get; set; }
    public string rut { get; set; }
    public string password { get; set; }
}
