import requests
import json

def enviar_json():
    # Crear el JSON con una cantidad dinámica de detalles
    detalles = []

    for i in range(1, 4):  # Cambia el número según la cantidad de detalles necesarios
        detalle = {
            "nrolinea": str(i),
            "cdgitem": [
                {"tpocodigo": "MONEDA", "vlrcodigo": "013"},
                {"tpocodigo": "TRANSACC", "vlrcodigo": "VENTA"}
            ],
            "undmditem": "UN",
            "nmbitem": "DOLAR USA",
            "qtyitem": "140315",
            "prcitem": "904",
            "descuentopct": "0",
            "descuentomonto": "0",
            "indexe": str(i),
            "montoitem": "126844760"
        }
        detalles.append(detalle)

    json_data = {
        "emisor": {
            "rutemisor": "76040308-3",
            "rsemisor": "EGGA INFORMATICA",
            "giroemisor": "SERVICIOS DE INGENIERIA",
            "actecoemisor": "429000",
            "diremisor": "quillay 467 villa presidente rio",
            "cmnaemisor": "TALCAHUANO",
            "ciuemisor": "CONCEPCION",
            "fecharesol": "2016-04-25",
            "numresol": "0",
            "codsiisucur": "1",
            "rutenvia": "13968481-8"
        },
        "receptor": {
            "rutreceptor": "77813960-K",
            "rsreceptor": "AMULEN CONSULTORES LTDA",
            "giroreceptor": "ASESORIA TRIBUTARIA",
            "dirreceptor": "URMENETA 305 OFIC 512",
            "cmnareceptor": "puerto montt",
            "ciureceptor": "puerto montt",
            "rutcaratula": "60803000-K"
        },
        "iddoc": {
            "tipodte": "34",
            "numdte": "3",
            "fechaemision": "2024-01-29",
            "frmapago": "1"
        },
        "referencia": {
            "codref": "0",
            "razonref": "DOCUMENTO DE PRUEBAS",
            "numref": "1",
            "TpoDocRef": "801",
            "folioref": "1",
            "fecharef": "2022-09-01"
        },
        "totales": {
            "montoexento": "126844760",
            "montototal": "126844760"
        },
        "detalle": detalles,
        "usuario": {
            "login": "eguenul",
            "rut": "13968481-8",
            "password": "amulen1956"
        }
    }

    return json.dumps(json_data)

def enviar_solicitud():
    url = "http://localhost:8080/appdtews/api/sendDTE"  # Reemplaza con la URL de tu servicio REST
    json_data = enviar_json()
    headers = {"Content-Type": "application/json"}

    response = requests.post(url, data=json_data, headers=headers)

    print("JSON enviado:")
    print(json_data)
    print("\nRespuesta del servicio:")
    print(response.text)

if __name__ == "__main__":
    enviar_solicitud()
