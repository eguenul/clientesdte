/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.egga.mavenproject1;

/**
 *
 * @author esteban
 */
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import okhttp3.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Mavenproject1 {

    public static void main(String[] args) throws IOException {
        String jsonData = getJsonData();
        System.out.println("JSON enviado:");
        System.out.println(jsonData);

        String apiUrl = "http://170.239.87.203:8080/appdtews/api/sendDTE";
        String response = enviarJson(apiUrl, jsonData);

        System.out.println("Respuesta del servicio:");
        System.out.println(response);
    }

    private static String getJsonData() {
        List<DetalleItem> detalles = new ArrayList<>();

        for (int i = 1; i <= 3; i++) {
            detalles.add(new DetalleItem(
                    String.valueOf(i),
                    List.of(new CodigoItem("MONEDA", "013"), new CodigoItem("TRANSACC", "VENTA")),
                    "UN",
                    "DOLAR USA",
                    "140315",
                    "904",
                    "0",
                    "0",
                    String.valueOf(i),
                    "126844760"
            ));
        }

        JsonData jsonData = new JsonData(
                new EmisorItem("76040308-3", "EGGA INFORMATICA", "SERVICIOS DE INGENIERIA", "429000",
                        "quillay 467 villa presidente rio", "TALCAHUANO", "CONCEPCION", "2016-04-25", "0", "1", "13968481-8"),
                new ReceptorItem("77813960-K", "AMULEN CONSULTORES LTDA", "ASESORIA TRIBUTARIA", "URMENETA 305 OFIC 512",
                        "puerto montt", "puerto montt", "60803000-K"),
                new IdDocItem("34", "1", "2024-01-29", "1"),
                new ReferenciaItem("0", "DOCUMENTO DE PRUEBAS", "1", "801", "1", "2022-09-01"),
                new TotalesItem("126844760", "126844760"),
                detalles,
                new UsuarioItem("DEMO", "1-9", "demo")
        );

        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        return gson.toJson(jsonData);
    }

    private static String enviarJson(String apiUrl, String jsonData) throws IOException {
        OkHttpClient client = new OkHttpClient();

        RequestBody body = RequestBody.create(jsonData, MediaType.parse("application/json"));
        Request request = new Request.Builder()
                .url(apiUrl)
                .post(body)
                .build();

        try (Response response = client.newCall(request).execute()) {
            return response.body().string();
        }
    }
}

class JsonData {
    EmisorItem emisor;
    ReceptorItem receptor;
    IdDocItem iddoc;
    ReferenciaItem referencia;
    TotalesItem totales;
    List<DetalleItem> detalle;
    UsuarioItem usuario;

    public JsonData(EmisorItem emisor, ReceptorItem receptor, IdDocItem iddoc, ReferenciaItem referencia, TotalesItem totales, List<DetalleItem> detalle, UsuarioItem usuario) {
        this.emisor = emisor;
        this.receptor = receptor;
        this.iddoc = iddoc;
        this.referencia = referencia;
        this.totales = totales;
        this.detalle = detalle;
        this.usuario = usuario;
    }
}

class EmisorItem {
    String rutemisor;
    String rsemisor;
    String giroemisor;
    String actecoemisor;
    String diremisor;
    String cmnaemisor;
    String ciuemisor;
    String fecharesol;
    String numresol;
    String codsiisucur;
    String rutenvia;

    public EmisorItem(String rutemisor, String rsemisor, String giroemisor, String actecoemisor, String diremisor, String cmnaemisor, String ciuemisor, String fecharesol, String numresol, String codsiisucur, String rutenvia) {
        this.rutemisor = rutemisor;
        this.rsemisor = rsemisor;
        this.giroemisor = giroemisor;
        this.actecoemisor = actecoemisor;
        this.diremisor = diremisor;
        this.cmnaemisor = cmnaemisor;
        this.ciuemisor = ciuemisor;
        this.fecharesol = fecharesol;
        this.numresol = numresol;
        this.codsiisucur = codsiisucur;
        this.rutenvia = rutenvia;
    }
}

class ReceptorItem {
    String rutreceptor;
    String rsreceptor;
    String giroreceptor;
    String dirreceptor;
    String cmnareceptor;
    String ciureceptor;
    String rutcaratula;

    public ReceptorItem(String rutreceptor, String rsreceptor, String giroreceptor, String dirreceptor, String cmnareceptor, String ciureceptor, String rutcaratula) {
        this.rutreceptor = rutreceptor;
        this.rsreceptor = rsreceptor;
        this.giroreceptor = giroreceptor;
        this.dirreceptor = dirreceptor;
        this.cmnareceptor = cmnareceptor;
        this.ciureceptor = ciureceptor;
        this.rutcaratula = rutcaratula;
    }
}

class IdDocItem {
    String tipodte;
    String numdte;
    String fechaemision;
    String frmapago;

    public IdDocItem(String tipodte, String numdte, String fechaemision, String frmapago) {
        this.tipodte = tipodte;
        this.numdte = numdte;
        this.fechaemision = fechaemision;
        this.frmapago = frmapago;
    }
}

class ReferenciaItem {
    String codref;
    String razonref;
    String numref;
    String tpodocref;
    String folioref;
    String fecharef;

    public ReferenciaItem(String codref, String razonref, String numref, String tpodocref, String folioref, String fecharef) {
        this.codref = codref;
        this.razonref = razonref;
        this.numref = numref;
        this.tpodocref = tpodocref;
        this.folioref = folioref;
        this.fecharef = fecharef;
    }
}

class TotalesItem {
    String montoexento;
    String montototal;

    public TotalesItem(String montoexento, String montototal) {
        this.montoexento = montoexento;
        this.montototal = montototal;
    }
}

class DetalleItem {
    String nrolinea;
    List<CodigoItem> cdgitem;
    String undmditem;
    String nmbitem;
    String qtyitem;
    String prcitem;
    String descuentopct;
    String descuentomonto;
    String indexe;
    String montoitem;

    public DetalleItem(String nrolinea, List<CodigoItem> cdgitem, String undmditem, String nmbitem, String qtyitem, String prcitem, String descuentopct, String descuentomonto, String indexe, String montoitem) {
        this.nrolinea = nrolinea;
        this.cdgitem = cdgitem;
        this.undmditem = undmditem;
        this.nmbitem = nmbitem;
        this.qtyitem = qtyitem;
        this.prcitem = prcitem;
        this.descuentopct = descuentopct;
        this.descuentomonto = descuentomonto;
        this.indexe = indexe;
        this.montoitem = montoitem;
    }
}

class CodigoItem {
    String tpocodigo;
    String vlrcodigo;

    public CodigoItem(String tpocodigo, String vlrcodigo) {
        this.tpocodigo = tpocodigo;
        this.vlrcodigo = vlrcodigo;
    }
}

class UsuarioItem {
    String login;
    String rut;
    String password;

    public UsuarioItem(String login, String rut, String password) {
        this.login = login;
        this.rut = rut;
        this.password = password;
    }
}
