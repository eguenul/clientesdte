#!/usr/bin/perl

use strict;
use warnings;
use JSON;
use HTTP::Request;
use LWP::UserAgent;
# JSON data


my $json_file = '/home/esteban/appdte/JSON/DTEVENTADOLARES.json';

# Leer el contenido del archivo JSON
open my $fh, '<', $json_file or die "No se pudo abrir el archivo $json_file: $!";
my $json_data = do { local $/; <$fh> };
close $fh;





# Decode JSON
my $data = decode_json($json_data);

# Accessing and printing some values
print "Emisor: " . $data->{emisor}->{rsemisor} . "\n";
print "Tipo de DTE: " . $data->{iddoc}->{tipodte} . "\n";
print "Receptor: " . $data->{receptor}->{rsreceptor} . "\n";
print "Total Monto: " . $data->{totales}->{montototal} . "\n";

# You can access other values similarly

# Add your processing logic here
my $url = 'http://localhost:8080/appdtews/api/sendDTE';

# Crear un objeto UserAgent
my $ua = LWP::UserAgent->new;

# Crear la solicitud HTTP POST con el JSON
my $request = HTTP::Request->new(POST => $url, ['Content-Type' => 'application/json'], $json_data);

# Enviar la solicitud y obtener la respuesta
my $response = $ua->request($request);

# Verificar si la solicitud fue exitosa
if ($response->is_success) {
    print "Solicitud exitosa. Respuesta:\n" . $response->decoded_content . "\n";
} else {
    print "Error en la solicitud. Código de respuesta: " . $response->code . "\n";
    print "Mensaje de error: " . $response->message . "\n";
}
